package com.accruent.alarm.worker;

import javax.baja.sys.NotRunningException;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.BWorker;
import javax.baja.util.CoalesceQueue;
import javax.baja.util.Worker;

public class BAlarmWorker
    extends BWorker
{
  /*-
   class BWorkerThread
   {
     properties
     {
     }
     actions
     {
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.worker.BWorkerThread(470402875)1.0$ @*/
/* Generated Mon Jun 26 09:21:08 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAlarmWorker.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


  private CoalesceQueue queue = null;
  private Worker worker = null;
  
  
  public Worker getWorker()
  {
    if (worker == null)
    {
        queue = new CoalesceQueue(1000);
        worker = new Worker(queue);
    }
    return worker;
  }
  
  public void postAsync(Runnable r)
  {
    if(!isRunning() || queue == null)
    {
      throw new NotRunningException();
    }
    queue.enqueue(r);
  }

}
