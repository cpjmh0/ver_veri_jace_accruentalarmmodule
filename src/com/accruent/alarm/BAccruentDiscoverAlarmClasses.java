package com.accruent.alarm;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.alarm.BAlarmClass;
import javax.baja.alarm.BAlarmService;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BComponent;
import javax.baja.sys.BLink;
import javax.baja.sys.BValue;
import javax.baja.sys.Context;
import javax.baja.sys.Flags;
import javax.baja.sys.LinkCheck;
import javax.baja.sys.Property;
import javax.baja.sys.Slot;
import javax.baja.sys.Sys;
import javax.baja.sys.Topic;
import javax.baja.sys.Type;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;

import com.accruent.alarm.worker.BAlarmWorker;

public class BAccruentDiscoverAlarmClasses
    extends BComponent
{
  /*-
   class BAccruentDiscoverAlarms
   {
     properties
     {
         asyncHandler : BAlarmWorker
         default {[new BAlarmWorker()]}
     }
     actions
     {
         discoverAlarmClasses() : BValue 
         flags {async}
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.BAccruentDiscoverAlarmClasses(277483863)1.0$ @*/
/* Generated Mon Jun 26 11:21:02 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>asyncHandler</code> property.
   * @see com.accruent.alarm.BAccruentDiscoverAlarmClasses#getAsyncHandler
   * @see com.accruent.alarm.BAccruentDiscoverAlarmClasses#setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BAlarmWorker(),null);
  
  /**
   * Get the <code>asyncHandler</code> property.
   * @see com.accruent.alarm.BAccruentDiscoverAlarmClasses#asyncHandler
   */
  public BAlarmWorker getAsyncHandler() { return (BAlarmWorker)get(asyncHandler); }
  
  /**
   * Set the <code>asyncHandler</code> property.
   * @see com.accruent.alarm.BAccruentDiscoverAlarmClasses#asyncHandler
   */
  public void setAsyncHandler(BAlarmWorker v) { set(asyncHandler,v,null); }

////////////////////////////////////////////////////////////////
// Action "discoverAlarmClasses"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>discoverAlarmClasses</code> action.
   * @see com.accruent.alarm.BAccruentDiscoverAlarmClasses#discoverAlarmClasses()
   */
  public static final Action discoverAlarmClasses = newAction(Flags.ASYNC,null);
  
  /**
   * Invoke the <code>discoverAlarmClasses</code> action.
   * @see com.accruent.alarm.BAccruentDiscoverAlarmClasses#discoverAlarmClasses
   */
  public BValue discoverAlarmClasses() { return (BValue)invoke(discoverAlarmClasses,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentDiscoverAlarmClasses.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  static Logger logger = Logger.getLogger("BAccruentDiscoverAlarmClasses.class");

  
  public BValue doDiscoverAlarmClasses()
  {
     BLink bAlarmLink = null;     
     BAccruentAlarmRecipient accruentAlarmRecipient = new BAccruentAlarmRecipient();
     BComponent bAlarms = null;
     logger.log(Level.INFO, "doDiscoverAlarmClasses");
     for(Slot slot : Sys.getService(BAlarmService.TYPE).getSlotsArray())
     {
      if(slot.isProperty() && slot.asProperty().getType().getTypeName().equals("AlarmClass"))
      {
        bAlarms = (BAlarmClass)Sys.getService(BAlarmService.TYPE).get(slot.getName());
        for (Topic topic : bAlarms.getTopicsArray())
        {
          if (topic.getName().equals("alarm"))
          {
            //bAlarmLink = new BLink(bAlarms, bAlarms.getTopic("alarm"), accruentAlarmRecipient.getAction("routeAlarm"));
            //accruentAlarmRecipient.add(String.format("Accruent%sLink", slot.getName()), bAlarmLink);
            //bAlarmLink.activate();
            
            //indirect method
            try
            {
               bAlarmLink = new BLink(BOrd.make(String.format("^|slot:/Services/AlarmService/%s", slot.getName())), "alarm", "routeAlarm", true);
               accruentAlarmRecipient.add(slot.getName(), bAlarmLink);
            }
            catch(Exception e)
            {
              logger.log(Level.SEVERE, e.getMessage());
            }
          }
        }
      }
     }
     for (BLink link : accruentAlarmRecipient.getLinks())
     {
       System.out.println(String.format("Link = %s", link.getName()));
     }
     return bAlarms;
  }
  
  
  public IFuture post(Action action, BValue argument, Context cx)
  {
    Invocation work = new Invocation(this, action, argument, cx);
    getAsyncHandler().postAsync(work);
    return null;
  }

}
