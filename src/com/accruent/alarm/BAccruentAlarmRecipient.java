package com.accruent.alarm;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.alarm.BAlarmRecipient;
import javax.baja.alarm.BAlarmRecord;
import javax.baja.alarm.ext.BAlarmSourceExt;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BComponent;
import javax.baja.sys.BFacets;
import javax.baja.sys.BRelTime;
import javax.baja.sys.BValue;
import javax.baja.sys.Clock;
import javax.baja.sys.Context;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.util.IFuture;
import javax.baja.util.Invocation;
import javax.baja.xml.XElem;
import javax.baja.xml.XText;
import javax.baja.xml.XWriter;
import javax.net.ssl.HttpsURLConnection;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.accruent.alarm.service.BAccruentDiscoverAlarmService;
import com.accruent.alarm.worker.BAlarmWorker;

public class BAccruentAlarmRecipient
    extends BAlarmRecipient
{
  /*-
   class BAccruentAlarmRecipient
   {
     properties
     {
         asyncHandler : BAlarmWorker
         default {[new BAlarmWorker()]}
     }
     actions
     {
          sendAlarmToAccruent(arg: BAlarmRecord) : BBoolean
          default {[new BAlarmRecord()]}
          flags {async, hidden}
          
          sendHeartBeatToAccruent() : BBoolean
          flags {async}
     }
     topics
     {
     
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.BAccruentAlarmRecipient(1256783495)1.0$ @*/
/* Generated Wed Jul 12 12:59:29 CDT 2017 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "asyncHandler"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>asyncHandler</code> property.
   * @see com.accruent.alarm.BAccruentAlarmRecipient#getAsyncHandler
   * @see com.accruent.alarm.BAccruentAlarmRecipient#setAsyncHandler
   */
  public static final Property asyncHandler = newProperty(0, new BAlarmWorker(),null);
  
  /**
   * Get the <code>asyncHandler</code> property.
   * @see com.accruent.alarm.BAccruentAlarmRecipient#asyncHandler
   */
  public BAlarmWorker getAsyncHandler() { return (BAlarmWorker)get(asyncHandler); }
  
  /**
   * Set the <code>asyncHandler</code> property.
   * @see com.accruent.alarm.BAccruentAlarmRecipient#asyncHandler
   */
  public void setAsyncHandler(BAlarmWorker v) { set(asyncHandler,v,null); }

////////////////////////////////////////////////////////////////
// Action "sendAlarmToAccruent"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>sendAlarmToAccruent</code> action.
   * @see com.accruent.alarm.BAccruentAlarmRecipient#sendAlarmToAccruent()
   */
  public static final Action sendAlarmToAccruent = newAction(Flags.ASYNC|Flags.HIDDEN,new BAlarmRecord(),null);
  
  /**
   * Invoke the <code>sendAlarmToAccruent</code> action.
   * @see com.accruent.alarm.BAccruentAlarmRecipient#sendAlarmToAccruent
   */
  public BBoolean sendAlarmToAccruent(BAlarmRecord arg) { return (BBoolean)invoke(sendAlarmToAccruent,arg,null); }

////////////////////////////////////////////////////////////////
// Action "sendHeartBeatToAccruent"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>sendHeartBeatToAccruent</code> action.
   * @see com.accruent.alarm.BAccruentAlarmRecipient#sendHeartBeatToAccruent()
   */
  public static final Action sendHeartBeatToAccruent = newAction(Flags.ASYNC,null);
  
  /**
   * Invoke the <code>sendHeartBeatToAccruent</code> action.
   * @see com.accruent.alarm.BAccruentAlarmRecipient#sendHeartBeatToAccruent
   */
  public BBoolean sendHeartBeatToAccruent() { return (BBoolean)invoke(sendHeartBeatToAccruent,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentAlarmRecipient.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/

  static Logger logger = Logger.getLogger("BAccruentAlarmRecipient.class");

  @Override
  public void handleAlarm(BAlarmRecord bAlarmRecord)
  {
   // logger.log(Level.INFO, String.format("**********handleAlarm", bAlarmRecord.getName()));
    Context context = this.getSession().getSessionContext();
    this.post(sendAlarmToAccruent, bAlarmRecord, context);
  }

  public IFuture post(Action action, BValue argument, Context cx)
  {
    Invocation work = new Invocation(this, action, argument, cx);
    getAsyncHandler().getWorker();
    getAsyncHandler().postAsync(work);
    return null;
  }
  
  public BBoolean doSendAlarmToAccruent(BAlarmRecord bAlarmRecord)
  {
    BBoolean bIsAlarmSent = BBoolean.make(true);
    BComponent bAccruentDiscoverAlarmServiceComponent = (BAccruentDiscoverAlarmService)Sys.getService(Sys.loadType(BAccruentDiscoverAlarmService.class));
    BAccruentDiscoverAlarmService bAccruentDiscoverAlarmService = (BAccruentDiscoverAlarmService) bAccruentDiscoverAlarmServiceComponent.asComponent();
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    XWriter xWriter = null;
    XElem xParent = null;
    XElem xHeadend = null;
    XElem xClient = null;
    XElem xAlarm = null;
    BFacets alarmFacets = bAlarmRecord.getAlarmData();
    String[] saAlarmInfo = null;
    String controllerName = null;
    String controllerDescription = null;
    String alarmReason = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    XMLGregorianCalendar xmlCal = null;
    Calendar alarmDate = null;
    String sAlarmDate = null;
    try
    {
       //get the alarm extension so we can use the ofNormalText 
       BOrd bord = BOrd.make(bAlarmRecord.getSource().toString());
       
       //SEVERE: Problem Sending Alarm com.tridium.nd.BNiagaraStation cannot be cast to javax.baja.alarm.ext.BAlarmSourceExt
       // local:|station:|slot:/Drivers/Software/Alarms/BoilerReturnTempLow/BoilerLowTemp
       //local:|station:|slot:/Drivers/NiagaraNetwork/BellHouseMCC03
       //alarmReason = base.getToOffnormalText().encodeToString();
       alarmReason =  bord.toString().split("local:")[1];
       
       //parse the alarm record first on pipe then on slash
       System.out.println(String.format("bAlarmRecord.source = %s", bAlarmRecord.getSource()));
       saAlarmInfo = bAlarmRecord.getSource().toString().split("\\|");
       saAlarmInfo = saAlarmInfo[saAlarmInfo.length - 1].split("/");
       controllerDescription = saAlarmInfo[saAlarmInfo.length - 2];
       controllerName = saAlarmInfo[saAlarmInfo.length - 4];
      
       //build the parent element site_controller_alarm
       xParent = new XElem("site_controller_alarm");
       xParent.addAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
       
       //removed the following line because nobs-rmstage was not happy with it.
       //xParent.addAttr("xsi:noNameSpaceSchema", "http://wbs.verisae.com/DataNett/xsd/siteControllerAlarmImport.xsd");
       
       //build the headend_element for the alarm
       xHeadend = buildXElem("headend_server", bAccruentDiscoverAlarmService.getHeadendServer(), true);

       //build the client element for the alarm
       xClient = new XElem("client");
       xClient.addContent(buildXElem("client_name", bAccruentDiscoverAlarmService.getClientName(), true));
       
       //build the alarm records
       xAlarm = new XElem("alarm");
       xAlarm.addContent(buildXElem("site_name", Sys.getStation().getStationName(), true));
       xAlarm.addContent(buildXElem("site_desc", Sys.getStation().getStationName(), true));
       xAlarm.addContent(buildXElem("controller_name", controllerName, true));
       xAlarm.addContent(buildXElem("controller_desc", controllerDescription, true));
       xAlarm.addContent(buildXElem("controller_device_type", "", false));
       xAlarm.addContent(buildXElem("serial", bAlarmRecord.getUuid().toString(), true));
       xAlarm.addContent(buildXElem("reason", alarmReason, true));
       xAlarm.addContent(buildXElem("description", String.format("%s - %s", Sys.getStation().getStationName(), controllerName), true));
       //xAlarm.addContent(buildXElem("description", bord.toString().split("local:")[1], true));
       //build the alarm date in UTC form
       alarmDate = Calendar.getInstance(TimeZone.getTimeZone(alarmFacets.get("TimeZone").toString().split(" ")[0]));
       logger.log(Level.INFO,String.format("***** BAlarmRecord Source State = %s", bAlarmRecord.getSourceState()));
       if(!bAlarmRecord.getSourceState().toString().trim().equals("Normal"))
       {
         xAlarm.addContent(buildXElem("status", "cleared", false));
         alarmDate.setTimeInMillis(bAlarmRecord.getNormalTime().getMillis());
         logger.log(Level.INFO, String.format("Site Cleared Alarm Date = %s", alarmDate));
       }
       else
       {
         xAlarm.addContent(buildXElem("status", "occurred", false));
         alarmDate.setTimeInMillis(bAlarmRecord.getTimestamp().getMillis());
         logger.log(Level.INFO, String.format("Site Occurred Alarm Date = %s", alarmDate));
       }
       sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
       sAlarmDate = sdf.format(alarmDate.getTime());
       xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(sAlarmDate);
       logger.log(Level.INFO, String.format("UTC Alarm Date = %s", xmlCal));
       xAlarm.addContent(buildXElem("tstamp", xmlCal.toString(), false));

       //add the alarm to the client
       xClient.addContent(xAlarm);

       //build the xmlWriter
       bos = new ByteArrayOutputStream();
       xWriter = new XWriter(bos);
       
       //build the output
       xParent.addContent(xHeadend);
       xParent.addContent(xClient);
       xParent.write(xWriter);
       xWriter.flush();
       xWriter.close();
       logger.log(Level.INFO, bos.toString());
       bIsAlarmSent = isAlarmSent(bAccruentDiscoverAlarmService.getWebURL(), bAccruentDiscoverAlarmService.getWebUserName(), bAccruentDiscoverAlarmService.getWebUserPassword(), bos.toString());
    }
    catch(Exception e)
    {
      bIsAlarmSent = BBoolean.make(false);
      logger.log(Level.SEVERE, String.format("Problem Sending Alarm %s", e.getMessage()));
    }
    return bIsAlarmSent;
  }

  public BBoolean doSendHeartBeatToAccruent()
  {
    BBoolean bIsAlarmSent = BBoolean.make(true);
    BComponent bAccruentDiscoverAlarmServiceComponent = (BAccruentDiscoverAlarmService)Sys.getService(Sys.loadType(BAccruentDiscoverAlarmService.class));
    BAccruentDiscoverAlarmService bAccruentDiscoverAlarmService = (BAccruentDiscoverAlarmService) bAccruentDiscoverAlarmServiceComponent.asComponent();
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    XWriter xWriter = null;
    XElem xParent = null;
    XElem xHeadend = null;
    XElem xClient = null;
    XElem xAlarm = null;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    XMLGregorianCalendar xmlCal = null;
    Calendar alarmDate = null;
    String sAlarmDate = null;
    try
    {
       //build the parent element site_controller_alarm
       xParent = new XElem("site_controller_alarm");
       xParent.addAttr("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
       
       //removed the following line because nobs-rmstage was not happy with it.
       //xParent.addAttr("xsi:noNameSpaceSchema", "http://wbs.verisae.com/DataNett/xsd/siteControllerAlarmImport.xsd");
       
       //build the headend_element for the alarm
       xHeadend = buildXElem("headend_server", bAccruentDiscoverAlarmService.getHeadendServer(), true);

       //build the client element for the alarm
       xClient = new XElem("client");
       xClient.addContent(buildXElem("client_name", bAccruentDiscoverAlarmService.getClientName(), true));
       
       //build the alarm records
       xAlarm = new XElem("alarm");
       xAlarm.addContent(buildXElem("site_name", Sys.getStation().getStationName(), true));
       xAlarm.addContent(buildXElem("site_desc", Sys.getStation().getStationName(), true));
       xAlarm.addContent(buildXElem("controller_name", "HEARTBEAT", true));
       xAlarm.addContent(buildXElem("controller_desc", "HEARTBEAT", true));
       xAlarm.addContent(buildXElem("controller_device_type", "", false));
       //build the alarm date in UTC form
       alarmDate = Calendar.getInstance();
       sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
       sAlarmDate = sdf.format(alarmDate.getTime());
       xAlarm.addContent(buildXElem("serial", String.valueOf(alarmDate.getTimeInMillis()), true));
       xAlarm.addContent(buildXElem("reason", "JACE Heartbeat", true));
       xAlarm.addContent(buildXElem("description", String.format("%s - %s", "JACE", "HEARTBEAT"), true));
       xAlarm.addContent(buildXElem("status", "occurred", false));
       xmlCal = DatatypeFactory.newInstance().newXMLGregorianCalendar(sAlarmDate);
       xAlarm.addContent(buildXElem("tstamp", xmlCal.toString(), false));
       logger.log(Level.INFO, String.format("UTC Alarm Date = %s", xmlCal));
       //add the alarm to the client
       xClient.addContent(xAlarm);

       //build the xmlWriter
       bos = new ByteArrayOutputStream();
       xWriter = new XWriter(bos);
       
       //build the output
       xParent.addContent(xHeadend);
       xParent.addContent(xClient);
       xParent.write(xWriter);
       xWriter.flush();
       xWriter.close();
     //  logger.log(Level.INFO, bos.toString());
       bIsAlarmSent = isAlarmSent(bAccruentDiscoverAlarmService.getWebURL(), bAccruentDiscoverAlarmService.getWebUserName(), bAccruentDiscoverAlarmService.getWebUserPassword(), bos.toString());
    }
    catch(Exception e)
    {
      bIsAlarmSent = BBoolean.make(false);
      logger.log(Level.SEVERE, String.format("Problem Sending Alarm %s", e.getMessage()));
    }
    return bIsAlarmSent;
  }

  
  
  XElem buildXElem(String elementName, String elementData, boolean isCdata) throws Exception
  {
     XElem xElem = new XElem(elementName);
     XText xText = new XText(elementData);
     xText.setCDATA(isCdata);
     xElem.addContent(xText);
     return xElem;
  }

  BBoolean isAlarmSent(String sUrl, String webUserName, String webUserPassword, String xmlAlarm) throws Exception
  {
    BBoolean isAlarmSent = BBoolean.make(true);
    DataInputStream dis = null;

    byte[] bIn = null;
    HttpURLConnection httpUrlConnection = null;
    HttpsURLConnection httpsUrlConnection = null;
    URL url = null;
    
    //Niagara has used a $ for a prefix to a hex character.  If we change that to a % sign we should be urlEncoded...
    xmlAlarm = xmlAlarm.replaceAll("\\$", "\\%");
    xmlAlarm = URLDecoder.decode(xmlAlarm, "UTF-8");
    
    //build the url
    sUrl  = String.format("%s?login=%s&password=%s&loginPage=webservice&xml=%s", sUrl, webUserName, webUserPassword, URLEncoder.encode(xmlAlarm, "UTF-8"));
    url = new URL(sUrl);
    //build the connection
    if(sUrl.startsWith("https"))
    {
       httpsUrlConnection = (HttpsURLConnection)url.openConnection();
       httpsUrlConnection.setRequestMethod("POST");
       httpsUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
       httpsUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       httpsUrlConnection.setRequestProperty("enctype", "multipart/form-data");
       httpsUrlConnection.setRequestProperty("Content-Length", "0");
       httpsUrlConnection.setDoInput(true);
       httpsUrlConnection.setDoOutput(true);
       httpsUrlConnection.connect();
       dis = new DataInputStream(httpsUrlConnection.getInputStream());
    }
    else
    {
       httpUrlConnection = (HttpURLConnection)url.openConnection();
       httpUrlConnection.setRequestMethod("POST");
       httpUrlConnection.setRequestProperty("User-Agent", "Mozilla/5.0");
       httpUrlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
       httpUrlConnection.setRequestProperty("enctype", "multipart/form-data");
       httpUrlConnection.setRequestProperty("Content-Length", "0");
       httpUrlConnection.setDoInput(true);
       httpUrlConnection.connect();
       dis = new DataInputStream(httpUrlConnection.getInputStream());
    }
    Thread.sleep(2000);
    while(dis.available() > 0)
    {
        bIn = new byte[dis.available()];
        dis.read(bIn);
      //  logger.log(Level.INFO, new String(bIn));
    }
    dis.close();
    return isAlarmSent;
  }
  @Override
  public void stationStarted()
  {
    //start the heartbeat process
    Clock.schedulePeriodically(this, BRelTime.makeMinutes(60), this.getAction("sendHeartBeatToAccruent"), null);
  }
}
