package com.accruent.alarm.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.baja.alarm.BAlarmClass;
import javax.baja.alarm.BAlarmService;
import javax.baja.file.BFileSystem;
import javax.baja.file.BIFile;
import javax.baja.file.FilePath;
import javax.baja.naming.BOrd;
import javax.baja.sys.Action;
import javax.baja.sys.BAbstractService;
import javax.baja.sys.BBoolean;
import javax.baja.sys.BComponent;
import javax.baja.sys.BLink;
import javax.baja.sys.BValue;
import javax.baja.sys.Flags;
import javax.baja.sys.Property;
import javax.baja.sys.Slot;
import javax.baja.sys.Sys;
import javax.baja.sys.Topic;
import javax.baja.sys.Type;

import com.accruent.alarm.BAccruentAlarmRecipient;

public class BAccruentDiscoverAlarmService
    extends BAbstractService
{
  /*-
   class BAccruentDiscoverAlarmService
   {
     properties
     {
        clientName : String
        default {[new String()]}

        headendServer : String
        default {[new String()]}
        
        webUserName : String
        default {[new String()]}
        
        webUserPassword : String
        default {[new String()]}
        
        webURL : String
        default {[new String()]}
        
        accruentAlarmRecipient : BAccruentAlarmRecipient
        default {[new BAccruentAlarmRecipient()]}
        
     }
     actions
     {
        discoverAlarmClasses() : BValue
        flags {summary}
        
        configureModule() : BValue
        flags {summary}
     }
     topics
     {
     }
   }
   -*/
/*+ ------------ BEGIN BAJA AUTO GENERATED CODE ------------ +*/
/*@ $com.accruent.alarm.service.BAccruentDiscoverAlarmService(857824474)1.0$ @*/
/* Generated Thu Jul 04 11:19:36 CDT 2019 by Slot-o-Matic 2000 (c) Tridium, Inc. 2000 */

////////////////////////////////////////////////////////////////
// Property "clientName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>clientName</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#getClientName
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#setClientName
   */
  public static final Property clientName = newProperty(0, new String(),null);
  
  /**
   * Get the <code>clientName</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#clientName
   */
  public String getClientName() { return getString(clientName); }
  
  /**
   * Set the <code>clientName</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#clientName
   */
  public void setClientName(String v) { setString(clientName,v,null); }

////////////////////////////////////////////////////////////////
// Property "headendServer"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>headendServer</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#getHeadendServer
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#setHeadendServer
   */
  public static final Property headendServer = newProperty(0, new String(),null);
  
  /**
   * Get the <code>headendServer</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#headendServer
   */
  public String getHeadendServer() { return getString(headendServer); }
  
  /**
   * Set the <code>headendServer</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#headendServer
   */
  public void setHeadendServer(String v) { setString(headendServer,v,null); }

////////////////////////////////////////////////////////////////
// Property "webUserName"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webUserName</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#getWebUserName
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#setWebUserName
   */
  public static final Property webUserName = newProperty(0, new String(),null);
  
  /**
   * Get the <code>webUserName</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#webUserName
   */
  public String getWebUserName() { return getString(webUserName); }
  
  /**
   * Set the <code>webUserName</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#webUserName
   */
  public void setWebUserName(String v) { setString(webUserName,v,null); }

////////////////////////////////////////////////////////////////
// Property "webUserPassword"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webUserPassword</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#getWebUserPassword
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#setWebUserPassword
   */
  public static final Property webUserPassword = newProperty(0, new String(),null);
  
  /**
   * Get the <code>webUserPassword</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#webUserPassword
   */
  public String getWebUserPassword() { return getString(webUserPassword); }
  
  /**
   * Set the <code>webUserPassword</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#webUserPassword
   */
  public void setWebUserPassword(String v) { setString(webUserPassword,v,null); }

////////////////////////////////////////////////////////////////
// Property "webURL"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>webURL</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#getWebURL
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#setWebURL
   */
  public static final Property webURL = newProperty(0, new String(),null);
  
  /**
   * Get the <code>webURL</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#webURL
   */
  public String getWebURL() { return getString(webURL); }
  
  /**
   * Set the <code>webURL</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#webURL
   */
  public void setWebURL(String v) { setString(webURL,v,null); }

////////////////////////////////////////////////////////////////
// Property "accruentAlarmRecipient"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>accruentAlarmRecipient</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#getAccruentAlarmRecipient
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#setAccruentAlarmRecipient
   */
  public static final Property accruentAlarmRecipient = newProperty(0, new BAccruentAlarmRecipient(),null);
  
  /**
   * Get the <code>accruentAlarmRecipient</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#accruentAlarmRecipient
   */
  public BAccruentAlarmRecipient getAccruentAlarmRecipient() { return (BAccruentAlarmRecipient)get(accruentAlarmRecipient); }
  
  /**
   * Set the <code>accruentAlarmRecipient</code> property.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#accruentAlarmRecipient
   */
  public void setAccruentAlarmRecipient(BAccruentAlarmRecipient v) { set(accruentAlarmRecipient,v,null); }

////////////////////////////////////////////////////////////////
// Action "discoverAlarmClasses"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>discoverAlarmClasses</code> action.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#discoverAlarmClasses()
   */
  public static final Action discoverAlarmClasses = newAction(Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>discoverAlarmClasses</code> action.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#discoverAlarmClasses
   */
  public BValue discoverAlarmClasses() { return (BValue)invoke(discoverAlarmClasses,null,null); }

////////////////////////////////////////////////////////////////
// Action "configureModule"
////////////////////////////////////////////////////////////////
  
  /**
   * Slot for the <code>configureModule</code> action.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#configureModule()
   */
  public static final Action configureModule = newAction(Flags.SUMMARY,null);
  
  /**
   * Invoke the <code>configureModule</code> action.
   * @see com.accruent.alarm.service.BAccruentDiscoverAlarmService#configureModule
   */
  public BValue configureModule() { return (BValue)invoke(configureModule,null,null); }

////////////////////////////////////////////////////////////////
// Type
////////////////////////////////////////////////////////////////
  
  public Type getType() { return TYPE; }
  public static final Type TYPE = Sys.loadType(BAccruentDiscoverAlarmService.class);

/*+ ------------ END BAJA AUTO GENERATED CODE -------------- +*/


  static Logger logger = Logger.getLogger("BAccruentDiscoverAlarmService.class");
  static String SEPARATOR = " : ";

  
  public BValue doConfigureModule()
  {
    BBoolean isConfigured = BBoolean.make("false");
    BufferedReader br = null;
    BIFile fAlarmConfig = BFileSystem.INSTANCE.findFile(new FilePath("^AccruentData/config/alarmConfig.ini"));
    String inRec = null;
    String key = null;
    String value = null;
    if(null != fAlarmConfig)
    {
      try
      {
         br = new BufferedReader(new InputStreamReader(fAlarmConfig.getInputStream()));
         while(br.ready())
         {
           inRec = br.readLine();
           if(inRec.contains(SEPARATOR))
           {
             key = inRec.split(SEPARATOR)[0];
             value = inRec.split(SEPARATOR)[1];
           }
           if(key.equals("ALARM_CLIENTNAME"))
           {
             setClientName(value);
           }
           else if(key.equals("ALARM_HEADENDNAME"))
           {
             setHeadendServer(value);
           }
           else if(key.equals("ALARM_URL"))
           {
             setWebURL(value);
           }
           else if(key.equals("ALARM_USERNAME"))
           {
             setWebUserName(value);
           }
           else if(key.equals("ALARM_PASSWORD"))
           {
             setWebUserPassword(value);
           }
         }
         isConfigured = BBoolean.make("true");
         setEnabled(Boolean.TRUE);
         Sys.getStation().save();
         this.start();
         br.close();
      }
      catch(Exception e)
      {
        logger.log(Level.SEVERE, String.format("Error Configuring Accruent Alarm Module %s", e.getMessage()));
      }
    }
    else
    {
      setEnabled(Boolean.FALSE);
      setClientName("");
      setHeadendServer("");
      setWebURL("");
      setWebUserName("");
      setWebUserPassword("");
      Sys.getStation().save();
      stop();
    }
    return isConfigured;
  }
 
  
  public BValue doDiscoverAlarmClasses()
  {
    BComponent accruentAlarmRecipientComponent = (BComponent)this.get("accruentAlarmRecipient");
    BAccruentAlarmRecipient accruentAlarmRecipient = (BAccruentAlarmRecipient)accruentAlarmRecipientComponent.asComponent();
    BLink bAlarmLink = null;     
    BComponent bAlarms = null;
    logger.log(Level.INFO, String.format("%s has %d links available", "doDiscoverAlarmClasses", accruentAlarmRecipient.getLinks().length));
    if (accruentAlarmRecipient.getLinks().length == 0)
    {
      for (Slot slot : Sys.getService(BAlarmService.TYPE).getSlotsArray())
      {
        if (slot.isProperty() && slot.asProperty().getType().getTypeName().equals("AlarmClass"))
        {
          bAlarms = (BAlarmClass) Sys.getService(BAlarmService.TYPE).get(slot.getName());
          for (Topic topic : bAlarms.getTopicsArray())
          {
            
            if (topic.getName().equals("alarm"))
            {
              /*indirect method*/
              try
              {
                 BOrd bOrd = bAlarms.getOrdInSession();
                 logger.log(Level.INFO,String.format("*******BOrd = %s", bOrd));
                 bAlarmLink = new BLink(bOrd, "alarm", "routeAlarm", true);
                 accruentAlarmRecipient.add(slot.getName(), bAlarmLink);
              }
              catch(Exception e)
              {
                e.printStackTrace();
                logger.log(Level.SEVERE, e.getMessage());
              }
            }
          }
        }
      }
    }
    for (BLink link : accruentAlarmRecipient.getLinks())
    {
      logger.log(Level.INFO, String.format("Link = %s", link.getName()));
    }
    return bAlarms;
  }
  
  @Override
  public void serviceStarted()
  {
    BBoolean isConfigured = (BBoolean)doConfigureModule(); 
    if(isConfigured.getBoolean())
    {  
       doDiscoverAlarmClasses();
    }   
    else
    {
      setEnabled(Boolean.FALSE);
      setClientName("");
      setHeadendServer("");
      setWebURL("");
      setWebUserName("");
      setWebUserPassword("");
      Sys.getStation().save();
      stop();
    }

  }
  
  @Override
  public void serviceStopped()
  {
    //deactivate the links activated
    logger.log(Level.INFO,"Removing Links");
    BComponent accruentAlarmRecipientComponent = (BComponent)this.get("accruentAlarmRecipient");
    BAccruentAlarmRecipient accruentAlarmRecipient = (BAccruentAlarmRecipient)accruentAlarmRecipientComponent.asComponent();
    for(BLink bLink : accruentAlarmRecipient.getLinks())
    {
       logger.log(Level.INFO,(String.format("Removing Link %s", bLink.getName())));
       accruentAlarmRecipient.remove(bLink);
    }
  }
  
  public Type[] getServiceTypes()
  {
    return new Type[] {TYPE};
  }
}
